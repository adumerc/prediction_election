import pandas as pd
import statsmodels.api as sm

# Charger le fichier csv
df = pd.read_csv('all_informations_with_election_mean.csv')

# Supprimer les colonnes non désirées
df = df.drop(['Code_departement', 'Libellé','Infractions','Population'], axis=1)

# Séparer les données en entrées (X) et sorties (y)
X = df.drop('Moyenne % Voix/Exp', axis=1)
y = df['Moyenne % Voix/Exp']

# Ajouter une colonne de constante pour l'ordonnée à l'origine
X = sm.add_constant(X)

# Créer un modèle de régression linéaire
model = sm.OLS(y, X)

# Entraîner le modèle
results = model.fit()

# Afficher le rapport de régression
print(results.summary())

