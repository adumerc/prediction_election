import pandas as pd

# Charger le fichier CSV
df_delinquance = pd.read_csv('delinquance-departement_year_21_modified.csv', sep=';', encoding='utf-8')

# Grouper les données par "Code.département" et calculer la somme des "faits"
grouped_data = df_delinquance.groupby('Code.département')['faits'].sum()

# Grouper les données par "Code.département" et sélectionner la première valeur de "POP"
population_data = df_delinquance.groupby('Code.département')['POP'].first()

# Convertir les résultats en un DataFrame
grouped_data_df = grouped_data.to_frame().reset_index()
population_data_df = population_data.to_frame().reset_index()

# Renommer les colonnes
grouped_data_df.columns = ['Code_departement', 'Infractions']
population_data_df.columns = ['Code_departement', 'Population']

# Fusionner les deux DataFrames en utilisant la colonne "Code_departement"
result_df = pd.merge(grouped_data_df, population_data_df, on='Code_departement')

# Enregistrer le DataFrame dans un nouveau fichier CSV
result_df.to_csv('infractions_departements.csv', index=False, encoding='utf-8')

print("Top 10 des Code.département ayant commis le plus de faits :")
print(result_df)
