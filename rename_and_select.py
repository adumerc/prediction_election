import pandas as pd

# Charger le fichier .xlsx
df = pd.read_csv("resultats_filtered.csv")

# Sélectionner les colonnes d'intérêt
cols_to_keep = ["Code du département", "% Abs/Ins", "% Voix/Exp","Unnamed: 32","Unnamed: 39","Unnamed: 46","Unnamed: 53",
                "Unnamed: 60","Unnamed: 67","Unnamed: 74","Unnamed: 81","Unnamed: 88","Unnamed: 95","Unnamed: 102"]
cols_renamed = {"Code du département":"Code_departement",
                "% Abs/Ins": "% Abs/Ins_1erTour",
                "% Voix/Exp": "ARTHAUD Nathalie",
                "Unnamed: 32": "ROUSSEL Fabien",
                "Unnamed: 39": "MACRON Emmanuel",
                "Unnamed: 46": "LASSALLE Jean",
                "Unnamed: 53": "LE PEN Marine",
                "Unnamed: 60": "ZEMMOUR Eric",
                "Unnamed: 67": "MELENCHON Jean-Luc",
                "Unnamed: 74": "HIDALGO Anne",
                "Unnamed: 81": "JADOT Yannick",
                "Unnamed: 88": "PECRESSE Valerie",
                "Unnamed: 95": "POUTOU Philippe",
                "Unnamed: 102": "DUPONT-AIGNAN Nicolas"}

# Renommer les colonnes d'intérêt
df.rename(columns=cols_renamed, inplace=True)

# Sélectionner les colonnes d'intérêt renommées
df = df[[cols_renamed[col] for col in cols_to_keep if col in cols_renamed]]

# Enregistrer les lignes filtrées dans un nouveau fichier CSV
df.to_csv("resultats_filtered_filtered.csv", index=False)