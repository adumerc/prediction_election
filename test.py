import pandas as pd

# Charger le fichier "all_informations.csv"
df_all = pd.read_csv("all_informations.csv")

# Charger le fichier "resultats_1er_tour_departement.csv"
df_resultats = pd.read_csv("resultats_1er_tour_departement.csv")

# Fusionner les deux fichiers en utilisant la colonne "Code_departement" comme clé
df_merged = pd.merge(df_all, df_resultats, on="Code_departement", how="left")

# Enregistrer le fichier fusionné dans un nouveau fichier CSV
df_merged.to_csv("all_informations_merged.csv", index=False)
