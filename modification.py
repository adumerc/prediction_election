import pandas as pd

# Charger le fichier Excel
df_election = pd.read_excel('election_results_without_etat_saisie-Npanneau.xlsx')

# Charger le fichier CSV
df_all_info = pd.read_csv('all_informations.csv')

# Renommer la colonne "Code du département" en "Code_departement" dans le fichier Excel
df_election = df_election.rename(columns={'Code du département': 'Code_departement'})

# Calculer la moyenne de la colonne "% Voix/Exp" pour chaque code département
df_election_mean = df_election.groupby('Code_departement')['% Voix/Exp'].mean().reset_index()
df_election_mean.columns = ['Code_departement', 'Moyenne % Voix/Exp']

# Fusionner les deux fichiers sur la colonne commune "Code_departement"
df_merged = pd.merge(df_all_info, df_election_mean, on='Code_departement', how='left')

# Enregistrer le fichier fusionné
df_merged.to_csv('all_informations_with_election_mean.csv', index=False)